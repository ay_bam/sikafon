<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="format-detection" content="Sikafon, Money, Transactions, Business, Businesses" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no, minimal-ui"/>

    <!-- fonts -->
    <link href="https://fonts.googleapis.com/css?family=Questrial|Raleway:700,900" rel="stylesheet">

    <link href="<?php echo e(asset('/css/bootstrap.min.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('/css/bootstrap.extension.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('/css/style.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('/css/swiper.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('/css/sumoselect.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('/css/font-awesome.min.css')); ?>" rel="stylesheet" type="text/css" />

    <link rel="shortcut icon" href="<?php echo e(asset('/img/favicon.ico')); ?>" />
    <title>Sikafon | Home</title>
</head>
<body>

<!-- LOADER -->
<div id="loader-wrapper"></div>

<div id="content-block">
    <!-- HEADER -->
    <header>
        <div class="header-top">
            <div class="content-margins">
                <div class="row">
                    <div class="col-md-5 hidden-xs hidden-sm">
                        <div class="entry"><b>contact us:</b> <a href="tel:+35235551238745">+3  (523) 555 123 8745</a></div>
                        <div class="entry"><b>email:</b> <a href="mailto:eafricgh.com">eafricgh@gmail.com</a></div>
                    </div>
                    <div class="col-md-7 col-md-text-right">
                        
                        <div class="entry"><a class="btn btn-sm" href="#"><b>login</b></a>&nbsp; or &nbsp;<a class="btn btn-sm" href="#"><b>register</b></a></div>
                        <div class="hamburger-icon">
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="header-bottom">
            <div class="content-margins">
                <div class="row">
                    <div class="col-xs-3 col-sm-1">
                        <a id="logo" href="<?php echo e(url('/')); ?>"><img src="img/sikalogo.png" alt="" /></a>
                    </div>
                    <div class="col-xs-9 col-sm-11 text-right">
                        <div class="nav-wrapper">
                            <div class="nav-close-layer"></div>
                            <nav>
                                <ul>
                                    <li class="active">
                                        <a href="<?php echo e(url('/')); ?>">Home</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo e(route('about')); ?>">about us</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo e(route('services')); ?>">Services</a>
                                    </li>
                                    <li><a href="<?php echo e(route('contact')); ?>">contact</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </header>

    <div class="header-empty-space"></div>

    <div class="content-margins grey">
        <div class="slider-wrapper">
            <div class="swiper-button-prev visible-lg"></div>
            <div class="swiper-button-next visible-lg"></div>
            <div class="swiper-container" data-parallax="1" data-auto-height="1">
                <div class="swiper-wrapper">
                    <div class="swiper-slide" style="background-image: url(img/slide-5.jpg);">
                        <div class="container">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="cell-view page-height">
                                        <div class="col-xs-b40 col-sm-b80"></div>
                                        <div data-swiper-parallax-x="-600">
                                            <h2 class="h1 light">Sika Payment</h2>
                                            <div class="title-underline light left"><span></span></div>
                                        </div>
                                        <div data-swiper-parallax-x="-600">
                                            <div class="col-xs-b5"></div>
                                        </div>

                                        <div class="col-xs-b40 col-sm-b80"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="empty-space col-xs-b80 col-sm-b0"></div>
                        </div>
                    </div>
                    <div class="swiper-slide" style="background-image: url(img/slide-3.jpg);">
                        <div class="container">
                            <div class="row">
                                <div class="col-sm-6 col-sm-offset-6 col-sm-text-right">
                                    <div class="cell-view page-height">
                                        <div class="col-xs-b40 col-sm-b80"></div>
                                        <div data-swiper-parallax-x="-600">
                                            <h2 class="h1 light">Sika Payment</h2>
                                            <div class="title-underline light left"><span></span></div>
                                        </div>
                                        <div data-swiper-parallax-x="-600">
                                            <div class="col-xs-b5"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="empty-space col-xs-b80 col-sm-b0"></div>
                        </div>
                    </div>

                    <div class="swiper-slide" style="background-image: url(img/slide-4.jpg);">
                        <div class="container">
                            <div class="row">
                                <div class="col-sm-6 col-sm-offset-6 col-sm-text-right">
                                    <div class="cell-view page-height">
                                        <div class="col-xs-b40 col-sm-b80"></div>
                                        <div data-swiper-parallax-x="-600">
                                            <h2 class="h1 light">Sika Payment</h2>
                                            <div class="title-underline light left"><span></span></div>
                                        </div>
                                        <div data-swiper-parallax-x="-600">
                                            <div class="col-xs-b5"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="empty-space col-xs-b80 col-sm-b0"></div>
                        </div>
                    </div>

                    <div class="swiper-slide" style="background-image: url(img/slide-2.jpg);">
                        <div class="container">
                            <div class="row">
                                <div class="col-sm-8 col-sm-offset-2 col-sm-text-center">
                                    <div class="cell-view page-height">
                                        <div class="col-xs-b40 col-sm-b80"></div>
                                        <div data-swiper-parallax-x="-600">
                                            <h2 class="h1 light">Sika Payment</h2>
                                            <div class="title-underline light left"><span></span></div>
                                        </div>
                                        <div data-swiper-parallax-x="-600">
                                            <div class="col-xs-b5"></div>
                                        </div>
                                        <div class="col-xs-b40 col-sm-b80"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="empty-space col-xs-b80 col-sm-b0"></div>
                        </div>
                    </div>

                    <div class="swiper-slide" style="background-image: url(img/slide-6.jpg);">
                        <div class="container">
                            <div class="row">
                                <div class="col-sm-8 col-sm-offset-2 col-sm-text-center">
                                    <div class="cell-view page-height">
                                        <div class="col-xs-b40 col-sm-b80"></div>
                                        <div data-swiper-parallax-x="-600">
                                            <h2 class="h1 light">Sika Payment</h2>
                                            <div class="title-underline light left"><span></span></div>
                                        </div>
                                        <div data-swiper-parallax-x="-600">
                                            <div class="col-xs-b5"></div>
                                        </div>
                                        <div class="col-xs-b40 col-sm-b80"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="empty-space col-xs-b80 col-sm-b0"></div>
                        </div>
                    </div>

                </div>
                <div class="swiper-pagination swiper-pagination-white"></div>
            </div>
        </div>


        <div class="row nopadding">

        </div>


        <div class="empty-space col-xs-b35 col-md-b70"></div>
        <div class="empty-space col-xs-b35 col-md-b70"></div>



        <div class="container-fluid">
            <div class="row">
                <div class="col-md-4 col-xs-b15 col-md-b0">
                    <div class="banner-shortcode style-4 rounded-image text-center" style="background-image: url(img/ad-1.jpg);">
                        <div class="valign-middle-cell">
                            <div class="valign-middle-content">
                                <div class="simple-article size-3 light transparent uppercase col-xs-b5">RELIABILITY</div>
                                <h3 class="h3 light">Most Reliable Payment Platform</h3>
                                <div class="title-underline light center"><span></span></div>
                                <div class="simple-article size-4 light transparent col-xs-b30">Our platform is live and online 24\7. Never fails and our customer services are always ready to help.</div>
                                <a class="button size-2 style-2" href="<?php echo e(route('services')); ?>">
                                        <span class="button-wrapper">
                                            <span class="icon"><img src="img/icon-1.png" alt=""></span>
                                            <span class="text">learn more</span>
                                        </span>
                                </a>
                            </div>
                        </div>
                        <div class="angle-left hidden-xs"></div>
                    </div>
                </div>
                <div class="col-md-4 col-xs-b15 col-md-b0">
                    <div class="banner-shortcode style-4 rounded-image text-center" style="background-image: url(img/ad-2.jpg);">
                        <div class="valign-middle-cell">
                            <div class="valign-middle-content">
                                <div class="simple-article size-3 light transparent uppercase col-xs-b5">high quality</div>
                                <h3 class="h3 light">State of the arts materials</h3>
                                <div class="title-underline light center"><span></span></div>
                                <div class="simple-article size-4 light transparent col-xs-b30">The Sika cards are made from high quality materials to ensure robustness. The platform also uses the best and most current
                                    technologies.</div>
                                <a class="button size-2 style-2" href="<?php echo e(route('services')); ?>">
                                        <span class="button-wrapper">
                                            <span class="icon"><img src="img/icon-1.png" alt=""></span>
                                            <span class="text">learn more</span>
                                        </span>
                                </a>
                            </div>
                        </div>
                        <div class="angle-left hidden-xs"></div>
                    </div>
                </div>
                <div class="col-md-4 col-xs-b15 col-md-b0">
                    <div class="banner-shortcode style-4 rounded-image text-center" style="background-image: url(img/ad-3.jpg);">
                        <div class="valign-middle-cell">
                            <div class="valign-middle-content">
                                <div class="simple-article size-3 light transparent uppercase col-xs-b5">convenience</div>
                                <h3 class="h3 light">satisfaction guarantted</h3>
                                <div class="title-underline light center"><span></span></div>
                                <div class="simple-article size-4 light transparent col-xs-b30">With speed, security, reliability and robustness guaranteed, sika card takes ease and convenience of use to the next level.</div>
                                <a class="button size-2 style-2" href="<?php echo e(route('services')); ?>">
                                        <span class="button-wrapper">
                                            <span class="icon"><img src="img/icon-1.png" alt=""></span>
                                            <span class="text">learn more</span>
                                        </span>
                                </a>
                            </div>
                        </div>
                        <div class="angle-left hidden-xs"></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="empty-space col-xs-b35 col-md-b70"></div>
        <div class="empty-space col-xs-b35 col-md-b70"></div>

        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-6">
                    <div class="cell-view simple-banner-height text-center">
                        <div class="empty-space col-sm-b35"></div>
                        <div class="simple-article grey uppercase size-5 col-xs-b5"><span class="color">special offers</span> for subscribers</div>
                        <h3 class="h3 col-xs-b15">free loan offers <span class="color">+</span> discount system <span class="color">+</span> best prices</h3>
                        <div class="simple-article size-3 col-xs-b25 col-sm-b50">Sika card subscribers are given the opportunity to quick loans, the system provides
                            discounts for every purchase made with sika card and also sika merchants provide the best prices for both goods and services. With sika
                        card, everything is just more cheaper and more affordable</div>
                        <div class="col-sm-12">
                            <div class="col-sm-12">
                                <div class="text-center">
                                    <a class="button size-2 style-2" href="<?php echo e(route('contact')); ?>">
                                        <span class="button-wrapper">
                                            <span class="icon"><img src="img/icon-4.png" alt=""></span>
                                            <span class="text">Contact Us</span>
                                        </span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="empty-space col-xs-b35"></div>
                    </div>
                </div>
            </div>
            <div class="row-background left hidden-xs">
                <img src="img/other.jpg" alt="" />
            </div>
        </div>
    </div>

    <div class="empty-space col-xs-b35 col-md-b70"></div>


    <!-- FOOTER -->
    <footer>
        <div class="container">
            <div class="footer-top">
                <div class="row">
                    <div class="col-sm-6 col-md-3 col-xs-b30 col-md-b0">
                        <h6 class="h6 light">Contact US</h6>
                        <div class="empty-space col-xs-b20"></div>
                        <div class="empty-space col-xs-b20"></div>
                        <div class="footer-contact"><i class="fa fa-mobile" aria-hidden="true"></i> contact us: <a href="tel:+35235551238745">+3  (523) 555 123 8745</a></div>
                        <div class="footer-contact"><i class="fa fa-envelope-o" aria-hidden="true"></i> email: <a href="mailto:office@exzo.com">office@exzo.com</a></div>
                        <div class="footer-contact"><i class="fa fa-map-marker" aria-hidden="true"></i> address: <a href="#">13th Akosombo Street, Airport, Accra</a></div>
                    </div>
                    <div class="col-sm-6 col-md-3 col-xs-b30 col-md-b0">
                        <h6 class="h6 light">quick links</h6>
                        <div class="empty-space col-xs-b20"></div>
                        <div class="footer-column-links">
                            <div class="row">
                                <div class="col-xs-6">
                                    <a href="<?php echo e(url('/')); ?>">home</a>
                                    <a href="<?php echo e(route('about')); ?>">about us</a>
                                    <a href="<?php echo e(route('services')); ?>">services</a>
                                    <a href="<?php echo e(route('contact')); ?>">contact</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6 col-md-3 col-xs-b30 col-md-b0">
                        <h6 class="h6 light">Other Services</h6>
                        <div class="empty-space col-xs-b20"></div>
                        <div class="footer-column-links">
                            <div class="row">
                                <div class="col-xs-6">
                                    <a href="http://jobs.areashoppers.co/" target="_blank">AreaJobs</a>
                                    <a href="http://areagrocery.areashoppers.co/index.php" target="_blank">AreaGrocery</a>
                                    <a href="http://areashoppers.co/" target="_blank">AreaShoppers</a>
                                    <a href="http://farmers.areashoppers.co/" target="_blank">Agric Solutions</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6 col-md-3 col-xs-b30 col-md-b0">
                        <h6 class="h6 light">T & C and Others</h6>
                        <div class="empty-space col-xs-b20"></div>
                        <div class="footer-column-links">
                            <div class="row">
                                <div class="col-xs-6">
                                    <a href="#">Terms & Conditions</a>
                                    <a href="#">Privacy policy</a>
                                    <a href="#">warranty</a>
                                    <a href="#">FAQS</a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="footer-bottom">
                <div class="row">
                    <div class="col-lg-8 col-xs-text-center col-lg-text-left col-xs-b20 col-lg-b0">
                        <div class="copyright">&copy; <?php echo date('Y')?> All rights reserved. Development by Sikafon Team | Powered By Phalcon Tech</div>
                    </div>
                </div>
            </div>
        </div>
    </footer>

</div>

</div>





<script src="<?php echo e(asset('/js/jquery-2.2.4.min.js')); ?>"></script>
<script src="<?php echo e(asset('/js/swiper.jquery.min.js')); ?>"></script>
<script src="<?php echo e(asset('/js/global.js')); ?>"></script>

<!-- styled select -->
<script src="<?php echo e(asset('/js/jquery.sumoselect.min.js')); ?>"></script>

<!-- counter -->
<script src="<?php echo e(asset('/js/jquery.classycountdown.js')); ?>"></script>
<script src="<?php echo e(asset('/js/jquery.knob.js')); ?>"></script>
<script src="<?php echo e(asset('/js/jquery.throttle.js')); ?>"></script>

</body>
</html>
