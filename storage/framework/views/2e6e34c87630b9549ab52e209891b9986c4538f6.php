<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="Sikafon" content="sika, sikafon, sikapeadjuma, money, transaction, transfer, travel, business" />
    <meta name="Sikafon" content="yes" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no, minimal-ui"/>

    <!-- fonts -->
    <link href="https://fonts.googleapis.com/css?family=Questrial|Raleway:700,900" rel="stylesheet">

    <link href="<?php echo e(asset('/css/bootstrap.min.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('/css/bootstrap.extension.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('/css/style.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('/css/swiper.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('/css/sumoselect.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('/css/font-awesome.min.css')); ?>" rel="stylesheet" type="text/css" />

    <link rel="shortcut icon" href="<?php echo e(asset('/img/favicon.ico')); ?>" />
    <title>Sikafon | Services</title>
</head>
<body>

<!-- LOADER -->
<div id="loader-wrapper"></div>

div id="content-block">
<!-- HEADER -->
<header>
    <div class="header-top">
        <div class="content-margins">
            <div class="row">
                <div class="col-md-5 hidden-xs hidden-sm">
                    <div class="entry"><b>contact us:</b> <a href="tel:+35235551238745">+3  (523) 555 123 8745</a></div>
                    <div class="entry"><b>email:</b> <a href="mailto:eafricgh.com">eafricgh@gmail.com</a></div>
                </div>
                <div class="col-md-7 col-md-text-right">
                    
                    <div class="entry"><a class="btn btn-sm" href="#"><b>login</b></a>&nbsp; or &nbsp;<a class="btn btn-sm" href="#"><b>register</b></a></div>
                    <div class="hamburger-icon">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="header-bottom">
        <div class="content-margins">
            <div class="row">
                <div class="col-xs-3 col-sm-1">
                    <a id="logo" href="<?php echo e(url('/')); ?>"><img src="img/sikalogo.png" alt="" /></a>
                </div>
                <div class="col-xs-9 col-sm-11 text-right">
                    <div class="nav-wrapper">
                        <div class="nav-close-layer"></div>
                        <nav>
                            <ul>
                                <li>
                                    <a href="<?php echo e(url('/')); ?>">Home</a>
                                </li>
                                <li>
                                    <a href="<?php echo e(route('about')); ?>">about us</a>
                                </li>
                                <li class="active">
                                    <a href="<?php echo e(route('services')); ?>">Services</a>
                                </li>
                                <li><a href="<?php echo e(route('contact')); ?>">contact</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>

</header>
    <div class="header-empty-space"></div>

    <div class="block-entry fixed-background" style="background-image: url(img/new-2.jpg);">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="cell-view simple-banner-height text-center">
                        <div class="empty-space col-xs-b35 col-sm-b70"></div>
                        <h1 class="h1 light">our services</h1>
                        <div class="title-underline center"><span></span></div>
                        <div class="simple-article light transparent size-4">Everything you need to know on the Sikafon payment platform and the sika cards</div>
                        <div class="empty-space col-xs-b35 col-sm-b70"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row nopadding">
        <div class="col-md-6">
            <div class="block-entry" style="background-image: url(img/ad-4.jpg);">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-2">
                            <div class="cell-view simple-banner-height middle text-center">
                                <div class="empty-space col-xs-b35 col-sm-b70"></div>
                                <div class="simple-article size-3 light transparent uppercase col-xs-b5">we offer</div>
                                <h2 class="h2 light">choose the best</h2>
                                <div class="title-underline light center"><span></span></div>
                                <div class="simple-article light transparent size-4">Payment is perhaps the most important aspect of transacting business. Therefore in choosing a payment platform,
                                always endeavor to choose the best. Why choose the rest when Sikafon is ahead of the <strong>current</strong> best?</div>
                                <div class="empty-space col-xs-b35 col-sm-b70"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1 col-lg-8 col-lg-offset-2">
                        <div class="cell-view simple-banner-height middle">
                            <div class="empty-space col-xs-b35 col-sm-b70"></div>
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="col-xs-text-center col-sm-text-left">
                                            <div class="simple-article size-2 uppercase color col-xs-b5" style="color: #000000; font-weight: bold;">discount</div>
                                            <h5 class="h5 col-xs-b5">loyality system</h5>
                                            <div class="simple-article size-2">Loyal subscribers and users are given discounts on goods and services</div>
                                        </div>
                                        <div class="empty-space col-xs-b30 col-sm-b60"></div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="col-xs-text-center col-sm-text-left">
                                            <div class="simple-article size-2 uppercase color col-xs-b5" style="color: #000000; font-weight: bold;">24/7</div>
                                            <h5 class="h5 col-xs-b5">customer support</h5>
                                            <div class="simple-article size-2">Ever welcoming and willing customer services ready to carter for ur every need</div>
                                        </div>
                                        <div class="empty-space col-xs-b30 col-sm-b60"></div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="col-xs-text-center col-sm-text-left">
                                            <div class="simple-article size-2 uppercase color col-xs-b5" style="color: #000000; font-weight: bold;">delivery system</div>
                                            <h5 class="h5 col-xs-b5">cross country</h5>
                                            <div class="simple-article size-2">Delivery even in the most remote parts of the country</div>
                                        </div>
                                        <div class="empty-space col-xs-b30 col-sm-b60"></div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="col-xs-text-center col-sm-text-left">
                                            <div class="simple-article size-2 uppercase color col-xs-b5" style="color: #000000; font-weight: bold;">smart support</div>
                                            <h5 class="h5 col-xs-b5">for every problem</h5>
                                            <div class="simple-article size-2">Providing I.T solutions for every problem you may face or need our help with</div>
                                        </div>
                                        <div class="empty-space col-xs-b30 col-sm-b60"></div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="col-xs-text-center col-sm-text-left">
                                            <div class="simple-article size-2 uppercase color col-xs-b5" style="color: #000000; font-weight: bold;">quality</div>
                                            <h5 class="h5 col-xs-b5">best materials</h5>
                                            <div class="simple-article size-2">Verified Sikafon merchants ensuring that all goods bought and sold are of the highest quality</div>
                                        </div>
                                        <div class="empty-space col-xs-b30 col-sm-b0"></div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="col-xs-text-center col-sm-text-left">
                                            <div class="simple-article size-2 uppercase color col-xs-b5" style="color: #000000; font-weight: bold;">professional staff</div>
                                            <h5 class="h5 col-xs-b5">over 50,000 AreaBosses</h5>
                                            <div class="simple-article size-2">Our AreaBosses situated in every district and every area across the country means that
                                            they are readily accessible for any information or sort out any problems you may be having with any of our services</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="empty-space col-xs-b35 col-sm-b70"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="empty-space col-xs-b35 col-md-b70"></div>
    <div class="empty-space col-xs-b35 col-md-b70"></div>

    <div class="container">
        <div class="text-center">
            <div class="simple-article size-3 grey uppercase col-xs-b5">our services</div>
            <div class="h2">what we do</div>
            <div class="title-underline center"><span></span></div>
        </div>
    </div>

    <div class="empty-space col-sm-b15 col-md-b50"></div>

    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <div class="icon-description-shortcode style-1">
                    <img class="icon-image" src="img/icon-22.png" alt="" />
                    <div class="title h6">Delivery</div>
                    <div class="description simple-article size-2">Our Area network permits the mode for secure delivery on purchases through the AreaBoss system.
                    This ensures that subscribers can buy from across nation and be confident in knowning that only are they buying verified quality goods but are also guaranteed
                     safe passage to them with any issues.</div>
                </div>
                <div class="empty-space col-xs-b40"></div>
            </div>
            <div class="col-sm-4">
                <div class="icon-description-shortcode style-1">
                    <img class="icon-image" src="img/icon-23.png" alt="" />
                    <div class="title h6">24 Hour Call Services</div>
                    <div class="description simple-article size-2">Our 24hrs call center allows users to call at anytime any day to report any inconvenience or
                    get information or receive clarity on any subject related to any of the services we provide. Your grievances are our loss and therefore we can only
                    make sure that whatever you need, you get.</div>
                </div>
                <div class="empty-space col-xs-b40"></div>
            </div>
            <div class="col-sm-4">
                <div class="icon-description-shortcode style-1">
                    <img class="icon-image" src="img/icon-24.png" alt="" />
                    <div class="title h6">Emergency Loans</div>
                    <div class="description simple-article size-2">As merchants or subscribers of Sikafon, we understand that it isn`t always that funding can be readily available
                     for you. That is why Sikafon provides <strong>Emergency Funds</strong> for you to enable you to continue that purchase or get that contract. It is all in your best interest.</div>
                </div>
                <div class="empty-space col-xs-b40"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <div class="icon-description-shortcode style-1">
                    <img class="icon" src="img/icon-25.png" alt="" />
                    <div class="title h6">Free Travel Tickets</div>
                    <div class="description simple-article size-2">As a thank you for using Sikafon and Sika Cards, we give a few of our <strong>Lucky</strong> subscribers free travel tickets. And guess it...
                    It doesn`t matter if you don`t fly because we provide both <strong>Land</strong> and <strong>Air</strong> tickets</div>
                </div>
                <div class="empty-space col-xs-b40"></div>
            </div>
            <div class="col-sm-4">
                <div class="icon-description-shortcode style-1">
                    <img class="icon" src="img/icon-26.png" alt="" />
                    <div class="title h6">Funding Schemes</div>
                    <div class="description simple-article size-2">For all for customer satisfaction and ensuring that subscribers are able to get access to funds to enable them transact businesses or purchase the services.
                    The funding scheme is a feature that is meant to provides reliable and easy money for subscribers. Terms and conditions apply to this as well.</div>
                </div>
                <div class="empty-space col-xs-b40"></div>
            </div>
            <div class="col-sm-4">
                <div class="icon-description-shortcode style-1">
                    <img class="icon" src="img/icon-27.png" alt="" />
                    <div class="title h6">Residual Income</div>
                    <div class="description simple-article size-2">Loyal customers, subscribers of sikafon and owners of Sika cards are given the opportunity to make passive or residual income from using Sikafon for business transactions.
                    Also when subscribers reference other users onto the system, they are awarded a percentile cash. Basically, keep using Sikafon and make money off of ypur purchases.</div>
                </div>
                <div class="empty-space col-xs-b40"></div>
            </div>
        </div>
    </div>

    <div class="empty-space col-sm-b15 col-md-b50"></div>

    <div class="container">
        <div class="text-center">
            <div class="simple-article size-3 grey uppercase col-xs-b5">our award</div>
            <div class="h2">Best customer support & satisfaction</div>
            <div class="title-underline center"><span></span></div>
        </div>
    </div>

    <div class="empty-space col-xs-b35 col-md-b70"></div>

    <div class="container">
        <div class="row vertical-aligned-columns">
            <div class="col-sm-5 col-xs-b30 col-sm-b0">
                <img class="image-thumbnail" src="img/thumbnail-34.jpg" alt="" />
            </div>
            <div class="col-sm-7">
                <div class="simple-article size-3">
                    <h4 class="h4">What Sikafon Represents</h4>
                    <p>
                        The Sikafon Card represents more than just an online payment system. Apart from the huge discounts and security
                        against online fraud offered to anyone who trans business on areashoppers.co using the Sika fon payment system,
                        continuous usage of the card guarantees the user a bouquet of mouth-watering opportunities.
                    </p>

                    <h4 class="h4">Beneficiaries of Sikafon</h4>
                    <p>
                        <ul>
                        <li>Students</li>
                        <li>Teachers</li>
                        <li>Hair Dressers</li>
                        <li>Petty Traders</li>
                        <li>Taxi / Commercial Drivers</li>
                        <li>Farmers</li>
                        <li>Nurses</li>
                        <li>Children</li>
                        <li>Corporate Organizations</li>
                        <li>Sole Proprietors</li>
                    </ul>
                    </p>
                </div>
            </div>
        </div>
    </div>

    <div class="empty-space col-xs-b35 col-md-b70"></div>
    <div class="empty-space col-xs-b35 col-md-b70"></div>

<!-- FOOTER -->
<footer>
    <div class="container">
        <div class="footer-top">
            <div class="row">
                <div class="col-sm-6 col-md-3 col-xs-b30 col-md-b0">
                    <h6 class="h6 light">Contact US</h6>
                    <div class="empty-space col-xs-b20"></div>
                    <div class="empty-space col-xs-b20"></div>
                    <div class="footer-contact"><i class="fa fa-mobile" aria-hidden="true"></i> contact us: <a href="tel:+35235551238745">+3  (523) 555 123 8745</a></div>
                    <div class="footer-contact"><i class="fa fa-envelope-o" aria-hidden="true"></i> email: <a href="mailto:office@exzo.com">office@exzo.com</a></div>
                    <div class="footer-contact"><i class="fa fa-map-marker" aria-hidden="true"></i> address: <a href="#">13th Akosombo Street, Airport, Accra</a></div>
                </div>
                <div class="col-sm-6 col-md-3 col-xs-b30 col-md-b0">
                    <h6 class="h6 light">quick links</h6>
                    <div class="empty-space col-xs-b20"></div>
                    <div class="footer-column-links">
                        <div class="row">
                            <div class="col-xs-6">
                                <a href="<?php echo e(url('/')); ?>">home</a>
                                <a href="<?php echo e(route('about')); ?>">about us</a>
                                <a href="<?php echo e(route('services')); ?>">services</a>
                                <a href="<?php echo e(route('contact')); ?>">contact</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-3 col-xs-b30 col-md-b0">
                    <h6 class="h6 light">Other Services</h6>
                    <div class="empty-space col-xs-b20"></div>
                    <div class="footer-column-links">
                        <div class="row">
                            <div class="col-xs-6">
                                <a href="http://jobs.areashoppers.co/" target="_blank">AreaJobs</a>
                                <a href="http://areagrocery.areashoppers.co/index.php" target="_blank">AreaGrocery</a>
                                <a href="http://areashoppers.co/" target="_blank">AreaShoppers</a>
                                <a href="http://farmers.areashoppers.co/" target="_blank">Agric Solutions</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-3 col-xs-b30 col-md-b0">
                    <h6 class="h6 light">T & C and Others</h6>
                    <div class="empty-space col-xs-b20"></div>
                    <div class="footer-column-links">
                        <div class="row">
                            <div class="col-xs-6">
                                <a href="#">Terms & Conditions</a>
                                <a href="#">Privacy policy</a>
                                <a href="#">warranty</a>
                                <a href="#">FAQS</a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="footer-bottom">
            <div class="row">
                <div class="col-lg-8 col-xs-text-center col-lg-text-left col-xs-b20 col-lg-b0">
                    <div class="copyright">&copy; <?php echo date('Y')?> All rights reserved. Developed by Sikafon Team | Powered By Phalcon Tech</div>
                </div>
            </div>
        </div>
    </div>
</footer>

</div>



<script src="<?php echo e(asset('/js/jquery-2.2.4.min.js')); ?>"></script>
<script src="<?php echo e(asset('/js/swiper.jquery.min.js')); ?>"></script>
<script src="<?php echo e(asset('/js/global.js')); ?>"></script>

<!-- styled select -->
<script src="<?php echo e(asset('/js/jquery.sumoselect.min.js')); ?>"></script>

<!-- counter -->
<script src="<?php echo e(asset('/js/jquery.classycountdown.js')); ?>"></script>
<script src="<?php echo e(asset('/js/jquery.knob.js')); ?>"></script>
<script src="<?php echo e(asset('/js/jquery.throttle.js')); ?>"></script>

</body>
</html>
