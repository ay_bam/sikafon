<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="format-detection" content="telephone=no" />
    <meta name="Sikafon" content="yes" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no, minimal-ui"/>

    <!-- fonts -->
    <link href="https://fonts.googleapis.com/css?family=Questrial|Raleway:700,900" rel="stylesheet">

    <link href="<?php echo e(asset('/css/bootstrap.min.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('/css/bootstrap.extension.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('/css/style.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('/css/swiper.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('/css/sumoselect.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('/css/font-awesome.min.css')); ?>" rel="stylesheet" type="text/css" />

    <link rel="shortcut icon" href="<?php echo e(asset('/img/favicon.ico')); ?>" />
    <title>Sikafon | About</title>
</head>
<body>

!-- LOADER -->
<div id="loader-wrapper"></div>

<div id="content-block">
    <!-- HEADER -->
    <header>
        <div class="header-top">
            <div class="content-margins">
                <div class="row">
                    <div class="col-md-5 hidden-xs hidden-sm">
                        <div class="entry"><b>contact us:</b> <a href="tel:+35235551238745">+3  (523) 555 123 8745</a></div>
                        <div class="entry"><b>email:</b> <a href="mailto:office@exzo.com">eafricgh@gmail.com</a></div>
                    </div>
                    <div class="col-md-7 col-md-text-right">
                        
                        <div class="entry"><a class="btn btn-sm" href="#"><b>login</b></a>&nbsp; or &nbsp;<a class="btn btn-sm" href="#"><b>register</b></a></div>
                        <div class="hamburger-icon">
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="header-bottom">
            <div class="content-margins">
                <div class="row">
                    <div class="col-xs-3 col-sm-1">
                        <a id="logo" href="<?php echo e(url('/')); ?>"><img src="img/sikalogo.png" alt="" /></a>
                    </div>
                    <div class="col-xs-9 col-sm-11 text-right">
                        <div class="nav-wrapper">
                            <div class="nav-close-layer"></div>
                            <nav>
                                <ul>
                                    <li>
                                        <a href="<?php echo e(url('/')); ?>">Home</a>
                                    </li>
                                    <li class="active">
                                        <a href="<?php echo e(route('about')); ?>">about us</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo e(route('services')); ?>">Services</a>
                                    </li>
                                    <li><a href="<?php echo e(route('contact')); ?>">contact</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </header>

    <div class="header-empty-space"></div>

    <div class="block-entry fixed-background" style="background-image: url(img/new-1.jpg);">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="cell-view simple-banner-height text-center">
                        <div class="empty-space col-xs-b35 col-sm-b70"></div>
                        <h1 class="h1 light">we are sikafon</h1>
                        <div class="title-underline center"><span></span></div>
                        <div class="simple-article light transparent size-4">Sikafon  Card is an e-cash payment system that allows users to process and store physical cash unto an electronic device
                        such as a mobile phone and made accessible for internet transaction.</div>
                        <div class="empty-space col-xs-b35 col-sm-b70"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="empty-space col-xs-b35 col-md-b70"></div>
    <div class="empty-space col-xs-b35 col-md-b70"></div>

    <div class="container">
        <div class="row">
            <div class="col-sm-5">
                <div class="simple-article size-3 grey uppercase col-xs-b5">about us</div>
                <div class="h2">Sikafon Cards</div>
                <div class="title-underline left"><span></span></div>
                <div class="simple-article size-4 grey"><strong>Sikafon</strong> is an electronic payment platform that works hand in hand by use of the <strong>Sika Card</strong> and its accompanying mobile app</div>
            </div>
            <div class="col-sm-7">
                <div class="simple-article size-3">
                    <p><strong>Sikafon Card</strong> is an e-cash payment system that allows users to process and store physical cash unto an electronic device such as a mobile phone or tablet and made accessible for internet transactions.</p>
                    <p><strong>A mobile application</strong> that comes with the <strong>Sikafon Card</strong> allows users to receive and send money online with notifications via <strong>sms</strong> to and from different mobile networks in Ghana for payments and other business transactions</p>
                </div>
            </div>
        </div>
    </div>

    <div class="empty-space col-xs-b35 col-md-b70"></div>

    <div class="container">
        <div class="slider-wrapper">
            <div class="swiper-button-prev hidden"></div>
            <div class="swiper-button-next hidden"></div>
            <div class="swiper-container" data-breakpoints="1" data-xs-slides="1" data-sm-slides="2" data-md-slides="2" data-lt-slides="3"  data-slides-per-view="3" data-space="30">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <div class="icon-description-shortcode style-2">
                            <img class="image-icon image-thumbnail rounded-image" src="img/nfc2.jpg" alt="" />
                            <div class="content">
                                <h6 class="title h6">Point Of Sale (POS)</h6>
                                <div class="description simple-article size-2">Sikacards can be used nation wide with all POS systems. Whether you are that the mall, library or at the movies, Sikafon and Sika Cards has got you down.</div>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="icon-description-shortcode style-2">
                            <img class="image-icon image-thumbnail rounded-image" src="img/qrcodes.jpg" alt="" />
                            <div class="content">
                                <h6 class="title h6">Smart Phones</h6>
                                <div class="description simple-article size-2">Sikafon runs on any android phone running version 4.0 and above. This means all transactions with merchants can be done directly from your android device even without the sikacard accompaniment. IOS and Windows to be released soon. </div>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="icon-description-shortcode style-2">
                            <img class="image-icon image-thumbnail rounded-image" src="img/nfc.jpg" alt="" />
                            <div class="content">
                                <h6 class="title h6">NFC Devices</h6>
                                <div class="description simple-article size-2">Sikafon and Sika Cards both use the Near Feild Technology (NFC), this enables even faster transaction rate and less time wasting during purchases. User convenience is assured.</div>
                            </div>
                        </div>
                    </div>

            </div>
        </div>
    </div>

    <div class="empty-space col-xs-b35 col-md-b70"></div>
    <div class="empty-space col-xs-b35 col-md-b70"></div>



    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-6">
                <div class="cell-view simple-banner-height big">
                    <div class="empty-space col-sm-b35"></div>
                    <div class="simple-article size-3 grey uppercase col-xs-b5">Real Time</div>
                    <div class="h2">Value for money</div>
                    <div class="title-underline left"><span></span></div>
                    <div class="simple-article size-4 grey">Sikafon and Sika Cards allows you to buy and sell without ever having to step inside a bank.</div>
                    <div class="empty-space col-xs-b30 col-sm-b70"></div>
                    <div class="icon-description-shortcode style-3">
                        <div class="image-icon">
                            <img class="image-thumbnail rounded-image" src="img/money.jpg" alt="" />
                        </div>
                        <div class="content">
                            <div class="cell-view">
                                <h6 class="title h6">Cash Agents</h6>
                                <div class="description simple-article size-2">Visit any cash agent and deposit your cash unto your Sika Card, check your balance in real time as it sends you notification of your recent activity.</div>
                            </div>
                        </div>
                    </div>
                    <div class="empty-space col-xs-b30"></div>
                    <div class="icon-description-shortcode style-3">
                        <div class="image-icon">
                            <img class="image-thumbnail rounded-image" src="img/qrcodes2.jpg" alt="" />
                        </div>
                        <div class="content">
                            <div class="cell-view">
                                <h6 class="title h6">QR Codes Transactions</h6>
                                <div class="description simple-article size-2">If a merchant doesn`t have a NFC device or Point Of Sale system, don`t let it worry you. Sikafon generates QR codes for merchants. This means you can simply scan the merchant`s QR code and make your transactions. The merchant would receive an sms notification immediately for the purchase to be made. Easy breezy.</div>
                            </div>
                        </div>
                    </div>
                    <div class="empty-space col-xs-b35"></div>
                </div>
            </div>
        </div>
        <div class="row-background left hidden-xs hidden-sm">
            <img src="img/fine.png" alt="" />
        </div>
    </div>
        
        <div class="empty-space col-xs-b35 col-md-b70"></div>
    </div>

        <!-- FOOTER -->
        <footer>
            <div class="container">
                <div class="footer-top">
                    <div class="row">
                        <div class="col-sm-6 col-md-3 col-xs-b30 col-md-b0">
                            <h6 class="h6 light">Contact US</h6>
                            <div class="empty-space col-xs-b20"></div>
                            <div class="empty-space col-xs-b20"></div>
                            <div class="footer-contact"><i class="fa fa-mobile" aria-hidden="true"></i> contact us: <a href="tel:+35235551238745">+3  (523) 555 123 8745</a></div>
                            <div class="footer-contact"><i class="fa fa-envelope-o" aria-hidden="true"></i> email: <a href="mailto:office@exzo.com">office@exzo.com</a></div>
                            <div class="footer-contact"><i class="fa fa-map-marker" aria-hidden="true"></i> address: <a href="#">13th Akosombo Street, Airport, Accra</a></div>
                        </div>
                        <div class="col-sm-6 col-md-3 col-xs-b30 col-md-b0">
                            <h6 class="h6 light">quick links</h6>
                            <div class="empty-space col-xs-b20"></div>
                            <div class="footer-column-links">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <a href="<?php echo e(url('/')); ?>">home</a>
                                        <a href="<?php echo e(route('about')); ?>">about us</a>
                                        <a href="<?php echo e(route('services')); ?>">services</a>
                                        <a href="<?php echo e(route('contact')); ?>">contact</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-6 col-md-3 col-xs-b30 col-md-b0">
                            <h6 class="h6 light">Other Services</h6>
                            <div class="empty-space col-xs-b20"></div>
                            <div class="footer-column-links">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <a href="http://jobs.areashoppers.co/" target="_blank">AreaJobs</a>
                                        <a href="http://areagrocery.areashoppers.co/index.php" target="_blank">AreaGrocery</a>
                                        <a href="http://areashoppers.co/" target="_blank">AreaShoppers</a>
                                        <a href="http://farmers.areashoppers.co/" target="_blank">Agric Solutions</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-6 col-md-3 col-xs-b30 col-md-b0">
                            <h6 class="h6 light">T & C and Others</h6>
                            <div class="empty-space col-xs-b20"></div>
                            <div class="footer-column-links">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <a href="#">Terms & Conditions</a>
                                        <a href="#">Privacy policy</a>
                                        <a href="#">warranty</a>
                                        <a href="#">FAQS</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="footer-bottom">
                    <div class="row">
                        <div class="col-lg-8 col-xs-text-center col-lg-text-left col-xs-b20 col-lg-b0">
                            <div class="copyright">&copy; <?php echo date('Y')?> All rights reserved. Developed by Sikafon Team | Powered By Phalcon Tech</div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>

</div>



<script src="<?php echo e(asset('/js/jquery-2.2.4.min.js')); ?>"></script>
<script src="<?php echo e(asset('/js/swiper.jquery.min.js')); ?>"></script>
<script src="<?php echo e(asset('/js/global.js')); ?>"></script>

<!-- styled select -->
<script src="<?php echo e(asset('/js/jquery.sumoselect.min.js')); ?>"></script>

<!-- counter -->
<script src="<?php echo e(asset('/js/jquery.classycountdown.js')); ?>"></script>
<script src="<?php echo e(asset('/js/jquery.knob.js')); ?>"></script>
<script src="<?php echo e(asset('/js/jquery.throttle.js')); ?>"></script>

</body>
</html>
