<?php

namespace App\Http\Controllers;

use App\Profile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\URL;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //
    }


    public function Profile()
    {
        return  view('profile');
    }

    public function addProfile(Request $request)
    {

        $this->validate($request,[
            'gender' => 'required',
            'dob' => 'required',
            'home_address' => 'required',
            'work_address' => 'required',
            'designation' => 'required',
            'profile_pic' => 'required'
        ]);

        $profiles = new Profile();
        $profiles->user_id = Auth::user()->id;
        $profiles->gender = $request->input('gender');
        $profiles->dob = $request->input('dob');
        $profiles->home_address = $request->input('home_address');
        $profiles->work_address = $request->input('work_address');
        $profiles->designation = $request->input('designation');
        if (Input::hasFile('profile_pic')){
            $file = Input::file('profile_pic');
            $file->move(public_path(). '/uploads/', $file->getClientOriginalName());
            $url = URL::to("/") . '/uploads/'. $file->getClientOriginalName();
        }
        $profiles->profile_pic = $url;
        $profiles->save();
        return redirect('/home')->with('response', 'Profile Updated Successfully');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */


    public function show(Profile $profile)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */


    public function edit(Profile $profile)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, Profile $profile)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */

    public function destroy(Profile $profile)
    {
        //
    }
}
