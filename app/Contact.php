<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Contact extends Model
{
    Use SoftDeletes;

    protected $fillable = [
        'name', 'email', 'phone', 'subject', 'message',
    ];


    public function setNameAttribute($value)
    {
        $this->attributes['name'] = ucfirst($value);
    }

    public function getNameAttribute($value)
    {
        return $value;
        return strtoupper($value);
    }

    public function getNumberAttribute($value)
    {
        return "0".$value;
    }

}
