<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    //Getting user information
    protected $fillable = [
        'user_id', 'gender', 'dob', 'home_address', 'work_address', 'designation', 'profile_pic',
    ];
}
