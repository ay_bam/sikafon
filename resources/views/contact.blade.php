<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="Sikafon" content="Sika, Sikafon, Fon, Money, Food, Transaction, Pay, Cash" />
    <meta name="Sikafone" content="yes" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no, minimal-ui"/>

    <!-- fonts -->
    <link href="https://fonts.googleapis.com/css?family=Questrial|Raleway:700,900" rel="stylesheet">

    <!-- Sweet Alert -->
    <link href="{{asset('/css/sweetalert2.min.css')}}" rel="stylesheet" type="text/css" />

    <link href="{{asset('/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('/css/bootstrap.extension.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('/css/style.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('/css/swiper.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('/css/sumoselect.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css" />

    <link rel="shortcut icon" href="{{asset('/img/favicon.ico')}}" />
    <title>Sikafon | Contact</title>
</head>
<body>

!-- LOADER -->
<div id="loader-wrapper"></div>

<div id="content-block">
    <!-- HEADER -->
    <header>
        <div class="header-top">
            <div class="content-margins">
                <div class="row">
                    <div class="col-md-5 hidden-xs hidden-sm">
                        <div class="entry"><b>contact us:</b> <a href="tel:+35235551238745">+3  (523) 555 123 8745</a></div>
                        <div class="entry"><b>email:</b> <a href="mailto:office@exzo.com">eafricgh@gmail.com</a></div>
                    </div>
                    <div class="col-md-7 col-md-text-right">
                        {{--<div class="entry"><a class="btn btn-sm" href="{{route('login')}}"><b>login</b></a>&nbsp; or &nbsp;<a class="btn btn-sm" href="{{route('register')}}"><b>register</b></a></div>--}}
                        <div class="entry"><a class="btn btn-sm" href="#"><b>login</b></a>&nbsp; or &nbsp;<a class="btn btn-sm" href="#"><b>register</b></a></div>
                        <div class="hamburger-icon">
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="header-bottom">
            <div class="content-margins">
                <div class="row">
                    <div class="col-xs-3 col-sm-1">
                        <a id="logo" href="{{url('/')}}"><img src="img/sikalogo.png" alt="" /></a>
                    </div>
                    <div class="col-xs-9 col-sm-11 text-right">
                        <div class="nav-wrapper">
                            <div class="nav-close-layer"></div>
                            <nav>
                                <ul>
                                    <li>
                                        <a href="{{url('/')}}">Home</a>
                                    </li>
                                    <li>
                                        <a href="{{route('about')}}">about us</a>
                                    </li>
                                    <li>
                                        <a href="{{route('services')}}">Services</a>
                                    </li>
                                    <li class="active"><a href="{{route('contact')}}">contact</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </header>

    <div class="header-empty-space"></div>

    <div class="block-entry fixed-background" style="background-image: url(img/new-3.jpg);">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="cell-view simple-banner-height text-center">
                        <div class="empty-space col-xs-b35 col-sm-b70"></div>
                        <h1 class="h1 light">contact us</h1>
                        <div class="title-underline center"><span></span></div>
                        <div class="simple-article light transparent size-4">For any information on this and any other product of ours or for furthur explanation of our services, these are our contact information</div>
                        <div class="empty-space col-xs-b35 col-sm-b70"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="empty-space col-xs-b35 col-md-b70"></div>
    <div class="empty-space col-xs-b35 col-md-b70"></div>

    <div class="container">
        <div class="text-center">
            <div class="simple-article size-3 grey uppercase col-xs-b5">our contacts</div>
            <div class="h2">we ready for your questions</div>
            <div class="title-underline center"><span></span></div>
        </div>
    </div>

    <div class="empty-space col-sm-b15 col-md-b50"></div>

    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <div class="icon-description-shortcode style-1">
                    <img class="icon" src="img/icon-25.png" alt="">
                    <div class="title h6">address</div>
                    <div class="description simple-article size-2">13 Akosombo Street, Accra , Airport</div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="icon-description-shortcode style-1">
                    <img class="icon" src="img/icon-23.png" alt="">
                    <div class="title h6">phone</div>
                    <div class="description simple-article size-2" style="line-height: 26px;">
                        <a href="tel:+35235551238745">+3  (523) 555 123 8745</a>
                        <br/>
                        <a href="tel:+35235557585238">+3  (523) 555 758 5238</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="icon-description-shortcode style-1">
                    <img class="icon" src="img/icon-28.png" alt="">
                    <div class="title h6">email</div>
                    <div class="description simple-article size-2"><a href="mailto:offce@exzo.com">eafricgh@gmail.com</a></div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="icon-description-shortcode style-1">
                    <img class="icon" src="img/icon-26.png" alt="">
                    <div class="title h6">Follow us</div>
                    <div class="follow light">
                        <a class="entry" href="#"><i class="fa fa-facebook"></i></a>
                        {{--<a class="entry" href="#"><i class="fa fa-twitter"></i></a>--}}
                        {{--<a class="entry" href="#"><i class="fa fa-linkedin"></i></a>--}}
                        {{--<a class="entry" href="#"><i class="fa fa-google-plus"></i></a>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="empty-space col-xs-b25 col-sm-b50"></div>

    <div class="container">
        <div class="map-wrapper">
            <div id="map-canvas" class="full-width" data-lat="5.611431" data-lng=" -0.181045" data-zoom="14"></div>
        </div>
        <div class="addresses-block hidden">
            <a class="marker" data-lat="34.0151244" data-lng="-118.4729871" data-string="1. Here is some address or email or phone or something else..."></a>
        </div>
    </div>

    <div class="empty-space col-xs-b25 col-sm-b50"></div>

    <div class="container">
        <h4 class="h4 text-center col-xs-b25">have any questions?</h4>
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                {!! Form::open(array('route' => 'contact.store', 'class' => 'contact-form'))!!}
                {{ csrf_field() }}
                    <div class="row m5">
                        <div class="col-sm-6">
                            <input class="simple-input col-xs-b20" type="text" value="" placeholder="Name" name="name" required/>
                        </div>
                        <div class="col-sm-6">
                            <input class="simple-input col-xs-b20" type="text" value="" placeholder="Email" name="email" required/>
                        </div>
                        <div class="col-sm-6">
                            <input class="simple-input col-xs-b20" type="text" value="" placeholder="Phone" name="phone" required/>
                        </div>
                        <div class="col-sm-6">
                            <input class="simple-input col-xs-b20" type="text" value="" placeholder="Subject" name="subject" required/>
                        </div>
                        <div class="col-sm-12">
                            <textarea class="simple-input col-xs-b20" placeholder="Your message" name="message" required></textarea>
                        </div>
                        <div class="col-sm-12">
                            <div class="text-center">
                                <div class="button size-2 style-2">
                                        <span class="button-wrapper">
                                            <span class="icon"><img src="img/icon-4.png" alt=""></span>
                                            <span class="text">Send Message</span>
                                        </span>
                                    <input type="submit"/>
                                </div>
                            </div>
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    <div class="empty-space col-xs-b35 col-md-b70"></div>
    <div class="empty-space col-xs-b35 col-md-b70"></div>

    <!-- FOOTER -->
    <footer>
        <div class="container">
            <div class="footer-top">
                <div class="row">
                    <div class="col-sm-6 col-md-3 col-xs-b30 col-md-b0">
                        <h6 class="h6 light">Contact US</h6>
                        <div class="empty-space col-xs-b20"></div>
                        <div class="empty-space col-xs-b20"></div>
                        <div class="footer-contact"><i class="fa fa-mobile" aria-hidden="true"></i> contact us: <a href="tel:+35235551238745">+3  (523) 555 123 8745</a></div>
                        <div class="footer-contact"><i class="fa fa-envelope-o" aria-hidden="true"></i> email: <a href="mailto:office@exzo.com">office@exzo.com</a></div>
                        <div class="footer-contact"><i class="fa fa-map-marker" aria-hidden="true"></i> address: <a href="#">13th Akosombo Street, Airport, Accra</a></div>
                    </div>
                    <div class="col-sm-6 col-md-3 col-xs-b30 col-md-b0">
                        <h6 class="h6 light">quick links</h6>
                        <div class="empty-space col-xs-b20"></div>
                        <div class="footer-column-links">
                            <div class="row">
                                <div class="col-xs-6">
                                    <a href="{{url('/')}}">home</a>
                                    <a href="{{route('about')}}">about us</a>
                                    <a href="{{route('services')}}">services</a>
                                    <a href="{{route('contact')}}">contact</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6 col-md-3 col-xs-b30 col-md-b0">
                        <h6 class="h6 light">Other Services</h6>
                        <div class="empty-space col-xs-b20"></div>
                        <div class="footer-column-links">
                            <div class="row">
                                <div class="col-xs-6">
                                    <a href="http://jobs.areashoppers.co/" target="_blank">AreaJobs</a>
                                    <a href="http://areagrocery.areashoppers.co/index.php" target="_blank">AreaGrocery</a>
                                    <a href="http://areashoppers.co/" target="_blank">AreaShoppers</a>
                                    <a href="http://farmers.areashoppers.co/" target="_blank">Agric Solutions</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6 col-md-3 col-xs-b30 col-md-b0">
                        <h6 class="h6 light">T & C and Others</h6>
                        <div class="empty-space col-xs-b20"></div>
                        <div class="footer-column-links">
                            <div class="row">
                                <div class="col-xs-6">
                                    <a href="#">Terms & Conditions</a>
                                    <a href="#">Privacy policy</a>
                                    <a href="#">warranty</a>
                                    <a href="#">FAQS</a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="footer-bottom">
                <div class="row">
                    <div class="col-lg-8 col-xs-text-center col-lg-text-left col-xs-b20 col-lg-b0">
                        <div class="copyright">&copy; <?php echo date('Y')?> All rights reserved. Developed by Sikafon Team | Powered By Phalcon Tech</div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div>



{{--Assests--}}
<script src="{{asset('/js/sweetalert2.min.js')}}"></script>
<!-- Include this after the sweet alert js file -->
@include('sweet::alert')

<script src="{{asset('/js/jquery-2.2.4.min.js')}}"></script>
<script src="{{asset('/js/swiper.jquery.min.js')}}"></script>
<script src="{{asset('/js/global.js')}}"></script>

<!-- styled select -->
<script src="{{asset('/js/jquery.sumoselect.min.js')}}"></script>

<!-- counter -->
<script src="{{asset('/js/jquery.classycountdown.js')}}"></script>
<script src="{{asset('js/jquery.knob.js')}}"></script>
<script src="{{asset('/js/jquery.throttle.js')}}"></script>

<!-- MAP -->
<script src="https://maps.googleapis.com/maps/api/js"></script>
<script src="{{asset('/js/map.js')}}"></script>




</body>

</html>
