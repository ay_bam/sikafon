<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="format-detection" content="Sikafon, Money, Transactions, Business, Businesses" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no, minimal-ui"/>

    <!-- fonts -->
    <link href="https://fonts.googleapis.com/css?family=Questrial|Raleway:700,900" rel="stylesheet">

    <link href="{{asset('/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('/css/bootstrap.extension.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('/css/style.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('/css/swiper.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('/css/sumoselect.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css" />

    <link rel="shortcut icon" href="{{asset('/img/favicon.ico')}}" />
    <title> Sikafon | Profile </title>
</head>
<body>

<!-- LOADER -->
<div id="loader-wrapper"></div>

<div id="content-block">
    <!-- HEADER -->
    <header>
        <div class="header-top">
            <div class="content-margins">
                <div class="row">
                    <div class="col-md-7 col-md-text-right">
                        <div class="hamburger-icon">
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="header-bottom">
            <div class="content-margins">
                <div class="row">
                    <div class="col-xs-3 col-sm-1">
                        <a id="logo" href="{{url('/')}}"><img src="img/sikalogo.png" alt="" /></a>
                    </div>
                    <div class="col-xs-9 col-sm-11 text-right">
                        <div class="nav-wrapper">
                            <div class="nav-close-layer"></div>
                            <nav>
                                <ul>

                                    <li class="active">
                                    @guest
                                        <li><a href="{{ route('login') }}">Login</a></li>
                                        <li><a href="{{ route('register') }}">Register</a></li>
                                        @else
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                                    {{ Auth::user()->name }} <span class="caret"></span>
                                                </a>

                                                <ul class="dropdown-menu" role="menu">
                                                    <li><a href="{{ route('home') }}">Home</a></li>
                                                    <li><a href="{{ route('login') }}">Account</a></li>
                                                    <li><a href="{{ route('login') }}">Checkout</a></li>
                                                    <li>
                                                        <a href="{{ route('logout') }}"
                                                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                                            Logout
                                                        </a>
                                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                            {{ csrf_field() }}
                                                        </form>
                                                    </li>
                                                </ul>
                                            </li>
                                            @endguest
                                            </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </header>

    <div class="header-empty-space"></div>
<br>
    {{--Profile Content--}}

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Profile Update</div>

                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" action="{{ url('/addProfile') }}" enctype="multipart/form-data">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">
                                <label for="gender" class="col-md-4 control-label">Gender</label>

                                <div class="col-md-6">
                                    <select class="form-control" name="gender">
                                        <option>Select Your Gender</option>
                                        <option value="Male">Male</option>
                                        <option value="Female">Female</option>
                                    </select>

                                    @if ($errors->has('gender'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('gender') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('dob') ? ' has-error' : '' }}">
                                <label for="dob" class="col-md-4 control-label">Date of Birth</label>

                                <div class="col-md-6">
                                    <input id="dob" type="date" name="dob" class="form-control" placeholder=" dd - mm - yyyy" name="dob" required>

                                    @if ($errors->has('dob'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('dob') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('home_address') ? ' has-error' : '' }}">
                                <label for="home_address" class="col-md-4 control-label">Home Address</label>

                                <div class="col-md-6">
                                    <input id="home_address" type="text" class="form-control" name="home_address" required>

                                    @if ($errors->has('home_address'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('home_address') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('work_address') ? ' has-error' : '' }}">
                                <label for="work_address" class="col-md-4 control-label">Work Address</label>

                                <div class="col-md-6">
                                    <input id="work_address" type="text" class="form-control" name="work_address" required>

                                    @if ($errors->has('work_address'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('work_address') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('designation') ? ' has-error' : '' }}">
                                <label for="designation" class="col-md-4 control-label">Profession</label>

                                <div class="col-md-6">
                                    <input id="designation" type="text" class="form-control" name="designation" required>

                                    @if ($errors->has('designation'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('designation') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('profile_pic') ? ' has-error' : '' }}">
                                <label for="profile_pic" class="col-md-4 control-label">Profile Picture</label>

                                <div class="col-md-6">
                                    <input id="profile_pic" type="file" class="form-control" name="profile_pic" required>

                                    @if ($errors->has('profile_pic'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('profile_pic') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Update
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>

</div>



<script src="{{asset('/js/jquery-2.2.4.min.js')}}"></script>
<script src="{{asset('/js/swiper.jquery.min.js')}}"></script>
<script src="{{asset('/js/global.js')}}"></script>

<!-- styled select -->
<script src="{{asset('/js/jquery.sumoselect.min.js')}}"></script>

<!-- counter -->
<script src="{{asset('/js/jquery.classycountdown.js')}}"></script>
<script src="{{asset('/js/jquery.knob.js')}}"></script>
<script src="{{asset('/js/jquery.throttle.js')}}"></script>

</body>
</html>
