<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="format-detection" content="Sikafon, Money, Transactions, Business, Businesses" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no, minimal-ui"/>

    <!-- fonts -->
    <link href="https://fonts.googleapis.com/css?family=Questrial|Raleway:700,900" rel="stylesheet">

    <link href="{{asset('/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('/css/bootstrap.extension.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('/css/style.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('/css/swiper.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('/css/sumoselect.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css" />

    <link rel="shortcut icon" href="{{asset('/img/favicon.ico')}}" />
    <title>Sikafon | Home</title>

    {{--styling--}}
    <style type="text/css">
        .avatar{
            border-radius: 100%;
            max-width:150px;
            max-height: 150px;
        }
    </style>

</head>
<body>

<!-- LOADER -->
<div id="loader-wrapper"></div>

<div id="content-block">
    <!-- HEADER -->
    <header>
        <div class="header-top">
            <div class="content-margins">
                <div class="row">
                    <div class="col-md-7 col-md-text-right">
                        <div class="hamburger-icon">
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="header-bottom">
            <div class="content-margins">
                <div class="row">
                    <div class="col-xs-3 col-sm-1">
                        <a id="logo" href="{{url('/')}}"><img src="img/sikalogo.png" alt="" /></a>
                    </div>
                    <div class="col-xs-9 col-sm-11 text-right">
                        <div class="nav-wrapper">
                            <div class="nav-close-layer"></div>
                            <nav>
                                <ul>

                                    <li class="active">
                                    @guest
                                        <li><a href="{{ route('login') }}">Login</a></li>
                                        <li><a href="{{ route('register') }}">Register</a></li>
                                        @else
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                                    {{ Auth::user()->name }} <span class="caret"></span>
                                                </a>

                                                <ul class="dropdown-menu" role="menu">
                                                    <li><a href="{{ route('profile') }}">Profile</a></li>
                                                    <li>
                                                        <a href="{{ route('logout') }}"
                                                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                                            Logout
                                                        </a>
                                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                            {{ csrf_field() }}
                                                        </form>
                                                    </li>
                                                </ul>
                                            </li>
                                            @endguest
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </header>

    <div class="header-empty-space"></div>
    <br>

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                @if(count($errors)>0)
                    @foreach($errors->all() as $error)
                        <div class="alert alert-danger alert-dismissible fade show">{{$error}}</div>
                    @endforeach
                @endif

                @if(session('response'))
                    <div class="alert alert-success alert-dismissible fade show">
                        {{session('response')}}
                    </div>
                @endif
                <div class="panel panel-warning">
                    <a class="btn btn-success pull-right" href="{{route('checkout')}}"> Transaction</a>
                    <div class="panel-heading"> Home | User Information  </div>
                                        
                    <div class="panel-body">
                        <div class="col-md-4">

                            {{--Profile Image--}}
                            @if(!empty($profile))
                                <img src="{{$profile->profile_pic}}" width="150px" height="150px" class="avatar" alt="">
                            @else
                                <img src="{{url('images/avatar.jpg')}}" class="avatar" alt="">
                            @endif

                        </div>
                        <div class="col-md-8">

                            <div class="lead"> Basic Information</div>
                            <hr>

                            {{--Profile Name--}}
                            @if(!empty($profile))
                                <p>User : {{$profile->name}}</p>
                                <br>
                            @else
                                <p>User : </p>
                                <br>
                            @endif

                            {{--Profile Gender--}}
                            @if(!empty($profile))
                                <p>Gender : {{$profile->gender}}</p>
                                <br>
                            @else
                                <p>Gender :</p>
                                <br>
                            @endif

                            {{--Profile DOB--}}
                            @if(!empty($profile))
                                <p>Date of Birth : {{$profile->dob}}</p>
                                <br>
                            @else
                                <p>Date of Birth : </p>
                                <br>
                            @endif

                            {{--Profile Designation--}}
                            @if(!empty($profile))
                                <p>Profession : {{$profile->designation}}</p>
                                <br>
                            @else
                                <p>Profession : </p>
                                <br>
                            @endif

                            {{--Profile Home--}}
                            @if(!empty($profile))
                                <p>Home Address : {{$profile->home_address}}</p>
                                <br>
                            @else
                                <p>Home Address : </p>
                                <br>
                            @endif

                            {{--Profile Designation--}}
                            @if(!empty($profile))
                                <p>Work Address : {{$profile->work_address}}</p>
                                <br>
                            @else
                                <p>Work Address : </p>
                                <br>
                            @endif
                            <br>
                            <hr>

                            <div class="lead"> Account Information</div>
                            <hr>

                            {{--Sikafon ID--}}
                            @if(!empty($profile))
                                <p>Sikafon ID: ************************</p>
                                <br>
                            @else
                                <p>Sikafon ID: ************************</p>
                                <br>
                            @endif

                            {{--Account  ID--}}
                            @if(!empty($profile))
                                <p>Account ID : ***********************</p>
                                <br>
                            @else
                                <p>Account ID : ***********************</p>
                                <br>
                            @endif

                            {{--Account Balance--}}
                            @if(!empty($profile))
                                <p>Account Balance : ******************</p>
                                <br>
                            @else
                                <p>Account Balance : ******************</p><br>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



</div>


<script type="javascript">
    $(".alert").alert()
</script>

<script src="{{asset('/js/jquery-2.2.4.min.js')}}"></script>
<script src="{{asset('/js/swiper.jquery.min.js')}}"></script>
<script src="{{asset('/js/global.js')}}"></script>

<!-- styled select -->
<script src="{{asset('/js/jquery.sumoselect.min.js')}}"></script>

<!-- counter -->
<script src="{{asset('/js/jquery.classycountdown.js')}}"></script>
<script src="{{asset('/js/jquery.knob.js')}}"></script>
<script src="{{asset('/js/jquery.throttle.js')}}"></script>

</body>
</html>
